var fs = require('fs');
var path = require('path');
var fileDir = process.argv[2];

fs.readdir(fileDir, function(error, files){
	if (error) throw error;
	
	var fileExt = "." + process.argv[3].toString();

	for (var i = 0; i < files.length; i++)
	{
		var ext = path.extname(files[i].toString());
		if (ext == fileExt)
		{
			console.log(files[i]);
		}
	}
});

// OR - ANSWER
// Didn't fully understand foreach

var fs = require('fs');
var path = require('path');

fs.readdir(process.argv[2], function(err, list){
	list.forEach(function(file){
		if (path.extname(file) === '.' + process.argv[3])
		{
			console.log(file);
		}
	})
	
});