// first arg (process.argv[2]) will be dir
// second arg (process.argv[3]) will be ext
var fs = require('fs');
var path = require('path');

module.exports = function (dir, ext, callback){
  var filteredFiles = [];
  ext = "." + ext;
  fs.readdir(dir, function (err, files) {
    if (err) return callback(err);

    files.forEach(function (element, index, array){
      if (path.extname(element) === ext){
        filteredFiles.push(element);
      }
    });
    return callback(null, filteredFiles);
  });
}
