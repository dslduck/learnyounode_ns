var http = require('http');
var bl = require('bl');
var async = require('async');

// three URLs provided...print data of each as string - in order called

async.series([
  function(callback){
    http.get(process.argv[2], function(res) {
      res.pipe(bl(function(err, data) {
        callback(null, data);
      }));
    });
  },
  function(callback){
    http.get(process.argv[3], function(res) {
      res.pipe(bl(function(err, data) {
        callback(null, data);
      }));
    });
  },
  function(callback){
    http.get(process.argv[4], function(res) {
      res.pipe(bl(function(err, data) {
        callback(null, data);
      }));
    });
  }
], function (err, results) {
  if (err) throw err;
  results.forEach(function(data) {
    console.log(data.toString());
  });
});
