// first arg (process.argv[2]) will be dir
// second arg (process.argv[3]) will be ext
var fs = require('fs');
var path = require('path');
var dir = process.argv[2];
var ext = "." +  process.argv[3];
fs.readdir(dir, function (err, files) {
  if (err) throw err;
  files.forEach(function (element, index, array){
    if (path.extname(element) === ext){
      console.log(element);
    }
  });
});
