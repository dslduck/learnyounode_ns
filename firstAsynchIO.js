// using callbacks to read a file asynchronously
var fs = require('fs');
fs.readFile(process.argv[2].toString(), writeNumLines);

function writeNumLines(error, bufferFileRead){
	console.log(bufferFileRead.toString().split("\n").length - 1);
}

// OR - Per answer provided

var fs = require('fs');
var file = process.argv[2]

fs.readFile(file, function(error, contents){
	var lines = contents.toString().split('\n').length -1;
	console.log(lines);
});