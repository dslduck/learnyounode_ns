// URL will be provided as first (progress.argv[2]) argument
var http = require('http');

var get = http.get(process.argv[2], function(res) {
  res.setEncoding('utf8');
  res.on('data', console.log);
  res.on('error', console.error);
});
