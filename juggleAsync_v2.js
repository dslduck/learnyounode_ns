var http = require('http');
var bl = require('bl');
var results = [];
var count = 0;
var numOfArgs = process.argv.length - 2;

function printResults() {
  for (var i = 0; i < numOfArgs; i++){
    console.log(results[i]);
  }
}

function httpGet(index) {
  http.get(process.argv[index + 2], function(response) {
    response.pipe(bl(function(err, data) {
      if (err) {
        return console.error(err);
      }

      results[index] = data.toString();
      count++;

      if (count == 3) {
        printResults();
      }
    }));
  });
}

for (var i = 0; i < numOfArgs; i++) {
  httpGet(i);
}
